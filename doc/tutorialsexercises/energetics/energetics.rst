.. _energetics:

==========
Energetics
==========

.. toctree::
   :maxdepth: 2

   cohesive_energy/cohesive_energy
   hubbardu/hubbardu
   defects/defects
   rpa_tut/rpa_tut
   rpa_ex/rpa
   fxc_correlation/rapbe_tut
